const express = require('express');
const router = express.Router();

function renderView(view, title) {
	return (req, res) => {
		res.render(view, {title});
	}
}

router.get('/', (req, res) => {
	res.redirect('/login');
});

router.get('/login', 			renderView('login', 'Iniciar sesión'));
router.get('/menu', 			renderView('menu', 'Menú de Opciones'));
router.get('/impresion', 	renderView('impresion', 'Nueva Impresión'));
router.get('/historial', 	renderView('historial', 'Historial'));
router.get('/mapa', 			renderView('mapa', 'Seleccionar punto de impresión'));
router.get('/detalle', 		renderView('detalle', 'Detalle de Impresión'));
router.get('/pago', 			renderView('pago', 'Seleccionar medio de pago'));

module.exports = router;